from rest_framework import serializers
from .models import Schedule, Slots, User
from datetime import date, datetime



class SlotsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slots
        fields = '__all__'


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        # fields = '__all__'
        fields = ['class_name', 'Teacher', 'Student', 'Date', 'TimeSlot']

    def __init__(self, *args, **kwargs):
        current_date = date.today()
        now = datetime.now()
        current_time = now.time()
        super(ScheduleSerializer, self).__init__(*args, **kwargs)
        self.fields['Teacher'].queryset = User.objects.filter(type='Teacher')
        self.fields['Student'].queryset = User.objects.filter(type='Student')
        self.fields['TimeSlot'].queryset = Slots.objects.exclude(
            id__in=Slots.objects.filter(schedule__Date=current_date, schedule__Student=3)).exclude(
            StartTime__lt=current_time)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

