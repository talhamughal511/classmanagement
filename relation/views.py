from .models import Slots, Schedule, User
from .serializers import SlotsSerializer, ScheduleSerializer, UserSerializer
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateAPIView
from datetime import date, datetime

# Create your views here.


class ClassList(ListAPIView):
    serializer_class = SlotsSerializer
    current_date = date.today()
    # serializer_class = ScheduleSerializer
    now = datetime.now()

    # current_time = now.strftime("%H:%M:%S")
    current_time = now.time()
    queryset = Slots.objects.exclude(id__in=Slots.objects.filter(schedule__Date=current_date, schedule__Student=3)).exclude(StartTime__lt=current_time)


class SlotsList(ListAPIView):
    serializer_class = SlotsSerializer
    queryset = Slots.objects.all()


class ScheduleClass(ListCreateAPIView):
    serializer_class = SlotsSerializer

    def get_queryset(self):
        current_date = date.today()
        now = datetime.now()

        current_time = now.strftime("%H:%M:%S")
        return Slots.objects.exclude(id__in=Slots.objects.filter(schedule__Date=current_date, schedule__Student=3)).exclude(StartTime__lt=current_time)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return SlotsSerializer
        else:
            return ScheduleSerializer


class RescheduleClass(RetrieveUpdateAPIView):
    queryset = Schedule.objects.filter(is_cancelled=True, Student=3)
    serializer_class = ScheduleSerializer


class CancelledClass(ListAPIView):
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.filter(is_cancelled=True, Student=3)