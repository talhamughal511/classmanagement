from django.urls import path
from .views import ClassList, SlotsList, RescheduleClass, ScheduleClass, CancelledClass


urlpatterns = [
    path('classlist/', ClassList.as_view()),
    path('slotlist/', SlotsList.as_view()),
    path('rescheduleclass/<int:pk>/', RescheduleClass.as_view()),
    path('scheduleclass/', ScheduleClass.as_view()),
    path('cancelledclass/', CancelledClass.as_view()),
    ]
