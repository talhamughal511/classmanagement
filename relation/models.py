from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class Slots(models.Model):
    StartTime = models.TimeField()
    EndTime = models.TimeField()

    def __int__(self):
         return self.StartTime


class User(AbstractUser):
    TYPE = {
        ("Student", "Student"),
        ("Teacher", "Teacher")
    }
    type = models.CharField(max_length=20, choices=TYPE)


class Schedule(models.Model):
    class_name = models.CharField(max_length=100)
    Teacher = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Teacher', null=True)
    Student = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Student', null=True)
    Date = models.DateField()
    Fees = models.PositiveIntegerField(default=20000)
    TimeSlot = models.ForeignKey(Slots, on_delete=models.CASCADE)
    is_cancelled = models.BooleanField(default=False)

    def __str__(self):
         return self.class_name
