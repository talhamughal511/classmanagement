from django.contrib import admin
from .models import Slots, Schedule, User

# Register your models here.

# admin.site.register(Classes)
admin.site.register(Slots)
# admin.site.register(Teacher)
# admin.site.register(Student)
admin.site.register(User)
admin.site.register(Schedule)